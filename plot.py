#!/usr/bin/env python

import sys
import pandas as pd
import numpy as np
from math import sqrt
import scipy.stats as sci
import matplotlib.pyplot as plt
import matplotlib

# Force matplotlib to not use any Xwindows backend.
#matplotlib.use('Agg')
import matplotlib.pyplot as plt

font = {'family': 'serif',
       'size': 28}

matplotlib.rc('text', usetex=True)
matplotlib.rc('font', **font)


def setup(width=1, height=None, span=False, params={}):
    if span:
        fig_width = 529.22128 / 72 # IEEE text width
    else:
        fig_width = 258.61064 / 72# IEEE column width

    if not height:
        golden_mean = (sqrt(5)-1.0)/2.0    # Aesthetic ratio
        fig_height = fig_width*golden_mean # height in inches
        print(fig_height)
    else:
        fig_height = height

    fig_width = fig_width * width

    # see http://matplotlib.org/users/customizing.html for more options
    rc = {'backend': 'ps',
              'text.usetex': True,
              'text.latex.preamble': ['\\usepackage{gensymb}'],
              'axes.labelsize': 8, # fontsize for x and y labels (was 10)
              'axes.titlesize': 8,
              'font.size': 8, # was 10
              'legend.fontsize': 8, # was 10
              'xtick.labelsize': 8,
              'ytick.labelsize': 8,
              'figure.figsize': [fig_width,fig_height],
              'font.family': 'serif',
              'figure.subplot.left': 0.15,
              'figure.subplot.right': 0.98,
              'figure.subplot.bottom': 0.17,
              'figure.subplot.top': 0.98,
              'savefig.dpi': 300
    }
    rc.update(params)

    matplotlib.rcParams.update(rc)


def main(source, dest):
    # parse the csv file
    df = pd.read_table(source, sep="\t", header=None,
                       names=["run", "relays", "loss", "gen", "size", "metric", "strategy", "feedback", "DOF", "sent_V", "sent_U", "rank_v", "rank_V", "rank_U"])

    # remove NaN values (last row of the log)
    df = df.dropna()

    # map the strategies to readable names
    strategies = ["\\texttt{%s}"%s for s in ["EXPECTATION", "NUMBER\\_RCVD", "PIVOTS", "MIN\\_MAX", "CCACK", "ALL\\_VECTORS"]]
    df.strategy = df.strategy.transform(lambda i: strategies[int(i)])

    # set an index from the field values
    alpha = (df.sent_V - (df.DOF / (1- df.loss))) / df.rank_U
    gamma = (df.DOF - df.rank_U) / df.DOF

    df = df.assign(alpha=alpha, gamma=gamma).set_index(["gen", "relays", "strategy", "feedback", "DOF", "run"])

    stats = df.groupby(df.index.names[:-1]).agg([np.mean, sci.sem])

    props = dict(ylim=[0, 0.15], xlabel="Feedback $n_f$", ylabel="Reliability $\\gamma$")
    setup(width=1, height=2.7193326747381223, params={"figure.subplot.top": 0.8})
    plot(dest+"/gamma_G8.png",  legend=True, data=stats.gamma.xs([4, 8, 8], level=["relays", "DOF", "gen"]).unstack("strategy"), **props)
    setup()
    plot(dest+"/gamma_G32.png",  data=stats.gamma.xs([4, 32, 32], level=["relays", "DOF", "gen"]).unstack("strategy"), **props)

    setup(width=1, height=2.7193326747381223, params={"figure.subplot.top": 0.8})
    plot(dest+"/gamma_R2.png",  legend=True, data=stats.gamma.xs([2, 16, 16], level=["relays", "DOF", "gen"]).unstack("strategy"), **props)
    setup()
    plot(dest+"/gamma_R3.png",   data=stats.gamma.xs([3, 16, 16], level=["relays", "DOF", "gen"]).unstack("strategy"), **props)
    plot(dest+"/gamma_R4.png",   data=stats.gamma.xs([4, 16, 16], level=["relays", "DOF", "gen"]).unstack("strategy"), **props)

    setup(width=1, height=2.7193326747381223, params={"figure.subplot.top": 0.8})
    plot(dest+"/gamma_DOF.png", legend=True, data=stats.gamma.xs([3, 8, 16], level=["relays", "DOF", "gen"]).unstack("strategy"), **props)
    setup()
    plot(dest+"/gamma_DOF2.png", data=stats.gamma.xs([3, 16, 16], level=["relays", "DOF", "gen"]).unstack("strategy"), **props)

    props = dict(ylim=[-.5, 3.0], xlabel="Feedback $n_f$", ylabel="Efficiency $\\alpha$")
    setup(width=1, height=2.7193326747381223, params={"figure.subplot.top": 0.8})
    plot(dest+"/alpha_G16.png", legend=True, data=stats.alpha.xs([4, 16, 16], level=["relays", "DOF", "gen"]).unstack("strategy"), **props)
    setup()
    plot(dest+"/alpha_G32.png",  data=stats.alpha.xs([4, 32, 32], level=["relays", "DOF", "gen"]).unstack("strategy"), **props)

    setup(width=1, height=2.7193326747381223, params={"figure.subplot.top": 0.8})
    plot(dest+"/alpha_R2.png",  legend=True, data=stats.alpha.xs([2, 32, 32], level=["relays", "DOF", "gen"]).unstack("strategy"), **props)
    setup()
    plot(dest+"/alpha_R3.png",   data=stats.alpha.xs([3, 32, 32], level=["relays", "DOF", "gen"]).unstack("strategy"), **props)
    plot(dest+"/alpha_R4.png",   data=stats.alpha.xs([4, 32, 32], level=["relays", "DOF", "gen"]).unstack("strategy"), **props)

def plot(name, data, legend=False, ylabel=None, xlabel="", **kwargs):
    data["mean"].plot.bar(yerr=data["sem"]*1.960, legend=legend, **kwargs)
    plt.grid(axis="y")
    if ylabel:
        plt.ylabel(ylabel)
    else:
        plt.yticks(visible=False)
    plt.xticks(rotation=0)
    plt.xlabel(xlabel)

    if legend:
        plt.legend(ncol=3, bbox_to_anchor=(-0.12, 1.29), loc=2, borderaxespad=0.)

    plt.savefig(name)

def overview(df):
    # fraction of missing rank
    missing = (df.rank_V - df.rank_U) / df.rank_V

    # forward efficiency
    efficiency = (df.sent_V - df.rank_V / (1 - df.loss)) / df.rank_U

    # feedback efficiency
    fbeff = df.sent_U / df.rank_V

    # combine the three columns for easier handling
    df = pd.DataFrame().assign(missing=missing, efficiency=efficiency, fbeff=fbeff)

    # group by index (except last field: run) and calculate mean and standard error per group
    stats = df.groupby(df.index.names[:-1]).agg([np.mean, sci.sem])

    # pivot to get separate columns for each strategy
    stats = stats.unstack("strategy")

    # iterate over the following columns
    cols = sorted(stats.index.get_level_values("relays").unique())
    # iterate over the following rows
    rows = sorted(stats.index.get_level_values("gen").unique())

    for field in df.columns.get_level_values(0):
        # create a figure with subfigures
        fig, axes = plt.subplots(ncols=len(cols)*2, nrows=len(rows), sharex=True, sharey=True)
        # subfigures for each combination of relays, gen and dof
        for x, relays in enumerate(cols):
            for y, gen in enumerate(rows):
                for i, dof in enumerate([gen, gen/2]):
                    # select correct subfigure
                    ax = axes[y, 2*x+i]
                    # select correct data
                    index = (relays, dof, gen)
                    data = stats.xs(index, level=["relays", "DOF", "gen"])[field]
                    # plot with y-error bars
                    data["mean"].plot.bar(ax=ax, yerr=data["sem"])
                    # set title and make a grid
                    ax.set_title("relays=%d DOF=%d gen=%d"%index)
                    ax.grid()

        # set figure title
        fig.suptitle(field)
        # display figure
        plt.show()
        #plt.savefig(field+".png")

if __name__ == "__main__":
    if len(sys.argv) == 1:
        main("projects/sim/temp.cvs.xz", "plots")
    elif len(sys.argv) == 2:
        main(sys.argv[1], "plots")
    else:
        main(sys.argv[1], sys.argv[2])
